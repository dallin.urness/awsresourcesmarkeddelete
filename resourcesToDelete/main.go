package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func readCsvFile(filePath string) [][]string {
	f, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Unable to read input file "+filePath, err)
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	records, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal("Unable to parse file as CSV for "+filePath, err)
	}

	return records
}

func getToDeleteFromCsv(files ...string) []ToDelete {
	const (
		ARN_TITLE_VAL     = "ARN"
		NAME_TITLE_VAL    = "Name"
		DELETE_TITLE_VAL  = "Plan to delete (Y/N)"
		ACCOUNT_TITLE_VAL = "Account"
		YES_VALUE         = "Y"
		DELETE_VALUE      = "D"
	)

	toDelete := make([]ToDelete, 0)

	for _, file := range files {
		parsedCsv := readCsvFile(file)
		var arnInd int
		var nameInd int
		var deleteInd int
		var accountInd int
		// find correct ind for each desired value
		for i := 0; i < len(parsedCsv[0]); i++ {
			if parsedCsv[0][i] == ARN_TITLE_VAL {
				arnInd = i
				continue
			}
			if parsedCsv[0][i] == NAME_TITLE_VAL {
				nameInd = i
				continue
			}
			if parsedCsv[0][i] == DELETE_TITLE_VAL {
				deleteInd = i
				continue
			}
			if parsedCsv[0][i] == ACCOUNT_TITLE_VAL {
				accountInd = i
				continue
			}
		}

		// find which objects need to be deleted
		for i := 1; i < len(parsedCsv); i++ {
			if parsedCsv[i][deleteInd] == YES_VALUE || parsedCsv[i][deleteInd] == DELETE_VALUE {
				arn := parsedCsv[i][arnInd]
				name := parsedCsv[i][nameInd]
				account := parsedCsv[i][accountInd]
				toDelete = append(toDelete, ToDelete{Arn: arn, Name: name, Account: account})
			}
		}
	}

	return toDelete
}

type ToDelete struct {
	Arn     string `json:"arn"`
	Name    string `json:"resource_name"`
	Account string `json:"account"`
}

func main() {
	toDeleteResources := getToDeleteFromCsv("../Orphaned Resources - 2023_01_06_Legacy.csv", "../Orphaned Resources - 2023_01_03 Org Accounts.csv")
	encoded, err := json.Marshal(toDeleteResources)
	if err != nil {
		fmt.Println("THERE WAS A PROBLEM")
	}

	err = ioutil.WriteFile("resources_marked_delete.json", encoded, 0644)
	if err != nil {
		fmt.Println("FAILED TO WRITE TO FILE")
	}
}
