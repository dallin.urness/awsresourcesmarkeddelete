package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func readCsvFile(filePath string) [][]string {
	f, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Unable to read input file "+filePath, err)
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	records, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal("Unable to parse file as CSV for "+filePath, err)
	}

	return records
}

func getToDeleteFromCsv(files ...string) []ResourceAssignedTo {
	const (
		ARN_TITLE_VAL         = "ARN"
		ASSIGNED_TO_TITLE_VAL = "Assigned To"
	)

	resourceAssignedTo := make([]ResourceAssignedTo, 0)

	for _, file := range files {
		parsedCsv := readCsvFile(file)
		var arnInd int
		var assignedToInd int
		// find correct ind for each desired value
		for i := 0; i < len(parsedCsv[0]); i++ {
			if parsedCsv[0][i] == ARN_TITLE_VAL {
				arnInd = i
				continue
			}
			if parsedCsv[0][i] == ASSIGNED_TO_TITLE_VAL {
				assignedToInd = i
				continue
			}
		}

		// find which objects need to be deleted
		for i := 1; i < len(parsedCsv); i++ {
			if parsedCsv[i][assignedToInd] != "" {
				arn := parsedCsv[i][arnInd]
				assignedTo := parsedCsv[i][assignedToInd]
				resourceAssignedTo = append(resourceAssignedTo, ResourceAssignedTo{Arn: arn, AssignedTo: assignedTo})
			}
		}
	}

	return resourceAssignedTo
}

type ResourceAssignedTo struct {
	Arn        string `json:"arn"`
	AssignedTo string `json:"assigned_to"`
}

func main() {
	toDeleteResources := getToDeleteFromCsv("../Orphaned Resources - Current.csv")
	encoded, err := json.Marshal(toDeleteResources)
	if err != nil {
		fmt.Println("THERE WAS A PROBLEM")
	}

	err = ioutil.WriteFile("resources_assigned_to.json", encoded, 0644)
	if err != nil {
		fmt.Println("FAILED TO WRITE TO FILE")
	}
}
